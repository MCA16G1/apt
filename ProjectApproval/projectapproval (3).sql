-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 19, 2018 at 04:34 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projectapproval`
--

-- --------------------------------------------------------

--
-- Table structure for table `abstract`
--

DROP TABLE IF EXISTS `abstract`;
CREATE TABLE IF NOT EXISTS `abstract` (
  `Acdyear` year(4) DEFAULT NULL,
  `YearofAdm` year(4) DEFAULT NULL,
  `ProjectName` varchar(50) DEFAULT NULL,
  `RollNo` varchar(10) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `AbstractNo` int(4) DEFAULT NULL,
  `AbstractFile` varchar(100) NOT NULL,
  `AdmNo` int(6) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  `StdMob` int(10) DEFAULT NULL,
  `RegNo` varchar(12) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `StudentName` varchar(100) NOT NULL,
  `guidecomments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abstract`
--

INSERT INTO `abstract` (`Acdyear`, `YearofAdm`, `ProjectName`, `RollNo`, `Date`, `AbstractNo`, `AbstractFile`, `AdmNo`, `Email`, `StdMob`, `RegNo`, `Status`, `StudentName`, `guidecomments`) VALUES
(2018, 2018, 'Book Store', '16BCA1069', '2018-11-09', 11020, 'Hostel_management_system1.docx', 100231, 'seetha@gmail.com', 2147483647, '11032', 'Rejected', 'seetha', ''),
(2018, 2018, 'testing', '16BCA1061', '2018-11-10', 11022, 'hello.html1.docx', 10222, 'seetha@gmail.com', 2147483647, '11032', 'Rejected', 'seetha', ''),
(2018, 2018, 'Hostel management system', '16BCA1062', '2018-11-15', 12, 'Hostel_management_system2.docx', 10235, 'anu@gmail.com', 2147483647, '11022', 'Pending', 'anu', 'ecEdvevfbvfd'),
(2018, 2018, 'MailServer Project', '16BCA1062', '2018-11-22', 11020, 'MailServer_Project.docx', 10222, 'anu@gmail.com', 2147483647, '11022', 'Pending', 'anu', 'approved'),
(2018, 2018, 'Hostel management system', '16BCA1061', '2018-11-17', 11022, 'Hostel_management_system3.docx', 10222, 'anu@gmail.com', 2147483647, '11022', 'Pending', 'anu', ''),
(2018, 2018, 'testing', '16BCA1060', '2018-11-15', 11020, 'MailServer_Project1.docx', 10222, 'seetha@gmail.com', 2147483647, '11032', 'Rejected', 'seetha', ''),
(2018, 2018, 'testing', '16BCA1061', '2018-11-15', 11022, 'MailServer_Project2.docx', 10222, 'seetha@gmail.com', 2147483647, '11032', 'Pending', 'seetha', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `ID` varchar(15) NOT NULL,
  `Username` varchar(15) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ID`, `Username`, `Password`, `Email`) VALUES
('1', 'rash', 'rash', 'rash@gmail');

-- --------------------------------------------------------

--
-- Table structure for table `guideallocation`
--

DROP TABLE IF EXISTS `guideallocation`;
CREATE TABLE IF NOT EXISTS `guideallocation` (
  `StudentID` varchar(25) DEFAULT NULL,
  `StudentName` varchar(25) DEFAULT NULL,
  `GuideID` varchar(25) DEFAULT NULL,
  `GuideName` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guideallocation`
--

INSERT INTO `guideallocation` (`StudentID`, `StudentName`, `GuideID`, `GuideName`) VALUES
('11022', 'anu', '11102', 'mithun'),
('11032', 'seetha', '11102', 'mithun');

-- --------------------------------------------------------

--
-- Table structure for table `guidenotification`
--

DROP TABLE IF EXISTS `guidenotification`;
CREATE TABLE IF NOT EXISTS `guidenotification` (
  `GuideId` varchar(15) DEFAULT NULL,
  `StudentId` varchar(15) DEFAULT NULL,
  `Message` varchar(100) DEFAULT NULL,
  `title` varchar(500) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guidenotification`
--

INSERT INTO `guidenotification` (`GuideId`, `StudentId`, `Message`, `title`, `status`) VALUES
('11102', '11032', 'testing', 'new message', 0),
('11102', '11032', 'project start date 20/2/2018', 'hi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `title` varchar(500) NOT NULL,
  `Message` varchar(100) DEFAULT NULL,
  `Guide` varchar(50) DEFAULT NULL,
  `Student` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`title`, `Message`, `Guide`, `Student`) VALUES
('new message', 'new submit date 2015', '1', '0'),
('new message', 'testing new data', '0', '1'),
('date', '12/12/2018', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
CREATE TABLE IF NOT EXISTS `registration` (
  `Rid` varchar(15) NOT NULL,
  `Fname` varchar(25) DEFAULT NULL,
  `Lname` varchar(25) DEFAULT NULL,
  `Usertype` char(10) DEFAULT NULL,
  `Year` year(4) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `Approvedstatus` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Rid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`Rid`, `Fname`, `Lname`, `Usertype`, `Year`, `Username`, `Password`, `Approvedstatus`) VALUES
('11002', 'Sabu', 'K', 'staff', 2018, 'sabu@gmail.com', 'sabu', 'approved'),
('11003', 'sabiq', 'km', 'student', 2018, 'sabiq@gmail.com', 'sabiq', 'pending'),
('11022', 'anu', 'vv', 'student', 2018, 'anu@gmail.com', 'anu', 'approved'),
('11023', 'ammu', 'mk', 'student', 2018, 'ammu@gmail.com', 'ammu', 'approved'),
('11032', 'seetha', 'K', 'student', 2018, 'seetha@gmail.com', 'seetha', 'approved'),
('11033', 'neenu', 'K', 'student', 2018, 'neenu@gmail.com', 'neenu', 'pending'),
('11102', 'mithun', 'K', 'staff', 2018, 'mithu@gmail.com', 'mithun', 'approved'),
('11122', 'appu', 'K', 'student', 2018, 'appu@gmail.com', 'appu', 'approved'),
('12121', 'sreetha', 'cv', 'staff', 2018, 'sreetha@gmail.com', 'sreetha', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `setdate`
--

DROP TABLE IF EXISTS `setdate`;
CREATE TABLE IF NOT EXISTS `setdate` (
  `startdate` date NOT NULL,
  `enddate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setdate`
--

INSERT INTO `setdate` (`startdate`, `enddate`) VALUES
('2018-11-19', '2018-11-24');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `Rid` varchar(20) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `Usertype` varchar(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`Rid`, `Username`, `Password`, `Usertype`) VALUES
('11102', NULL, NULL, 'guide'),
('12121', NULL, NULL, 'guide');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
