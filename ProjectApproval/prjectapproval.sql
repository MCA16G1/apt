/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.7.19 : Database - projectapproval
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`projectapproval` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `projectapproval`;

/*Table structure for table `abstract` */

DROP TABLE IF EXISTS `abstract`;

CREATE TABLE `abstract` (
  `Acdyear` year(4) DEFAULT NULL,
  `YearofAdm` year(4) DEFAULT NULL,
  `ProjectName` varchar(50) DEFAULT NULL,
  `RollNo` varchar(10) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `AbstractNo` int(4) DEFAULT NULL,
  `AbstractFile` varchar(100) NOT NULL,
  `AdmNo` int(6) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  `StdMob` int(10) DEFAULT NULL,
  `RegNo` varchar(12) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `StudentName` varchar(100) NOT NULL,
  `guidecomments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `abstract` */

insert  into `abstract`(`Acdyear`,`YearofAdm`,`ProjectName`,`RollNo`,`Date`,`AbstractNo`,`AbstractFile`,`AdmNo`,`Email`,`StdMob`,`RegNo`,`Status`,`StudentName`,`guidecomments`) values (2018,2018,'Hostel management system','16BCA1069','2018-11-23',11022,'Hostel_management_system11.docx',100231,'seetha@gmail.com',2147483647,'11032','Approved','seetha',''),(2018,2018,'Hostel management system','16BCA1069','2018-11-25',11022,'Hostel_management_system11.docx',100231,'sabu@gmail.com',2147483647,'11002','Approved','Sabu',''),(2018,2018,'MailServer Project','16BCA1069','2018-11-26',1,'MailServer_Project3.docx',100231,'seetha@gmail.com',2147483647,'11032','Rejected','seetha',''),(2018,2018,'Hostel management system','16BCA1061','2018-11-26',11,'Hostel_management_system21.docx',100231,'seetha@gmail.com',2147483647,'11032','Rejected','seetha',''),(2018,2018,'iot','mes16','2018-11-25',1,'An_IoT.docx',1000,'sabiq@gmail.com',1234567890,'11003','Approved','sabiq',''),(2019,2018,'hhhhhhhhhhh','mes17','2018-11-27',1,'hhhhhhhh.docx',10000,'ammu@gmail.com',987654321,'11023','Rejected','ammu',''),(2019,2018,'bigdata','mes18','2018-11-28',1,'bigdata.docx',100000,'anu@gmail.com',2147483647,'11022','pending','anu','approved');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `ID` varchar(15) NOT NULL,
  `Username` varchar(15) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`ID`,`Username`,`Password`,`Email`) values ('1','rash','rash','rash@gmail');

/*Table structure for table `guideallocation` */

DROP TABLE IF EXISTS `guideallocation`;

CREATE TABLE `guideallocation` (
  `StudentID` varchar(25) DEFAULT NULL,
  `StudentName` varchar(25) DEFAULT NULL,
  `GuideID` varchar(25) DEFAULT NULL,
  `GuideName` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `guideallocation` */

insert  into `guideallocation`(`StudentID`,`StudentName`,`GuideID`,`GuideName`) values ('11032','seetha','1000','Geevar'),('MES16MCA4','Anjana','1000','Geevar'),('11023','ammu','1002','Noushad'),('MES16MCA17','Haritha','1002','Noushad'),('mes4','Anjana','1002','Noushad'),('11003','sabiq','11102','mithun'),('11022','anu','11102','mithun'),('11033','neenu','11102','mithun'),('11122','appu','11102','mithun');

/*Table structure for table `guidenotification` */

DROP TABLE IF EXISTS `guidenotification`;

CREATE TABLE `guidenotification` (
  `GuideId` varchar(15) DEFAULT NULL,
  `StudentId` varchar(15) DEFAULT NULL,
  `Message` varchar(100) DEFAULT NULL,
  `title` varchar(500) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `guidenotification` */

insert  into `guidenotification`(`GuideId`,`StudentId`,`Message`,`title`,`status`) values ('11102','11032','testing','new message',0),('11102','11032','project start date 20/2/2018','hi',0);

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `title` varchar(500) NOT NULL,
  `Message` varchar(100) DEFAULT NULL,
  `Guide` varchar(50) DEFAULT NULL,
  `Student` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

insert  into `notification`(`title`,`Message`,`Guide`,`Student`) values ('new message','new submit date 2015','1','0'),('new message','testing new data','0','1'),('date','12/12/2018','0','1'),('dg','dsagdsg','0','1'),('submission','ffffffffffffffgggggggggggj\r\nfffffff','1','1'),('due date','25/11/18 to 30/11/18','1','1');

/*Table structure for table `registration` */

DROP TABLE IF EXISTS `registration`;

CREATE TABLE `registration` (
  `Rid` varchar(15) NOT NULL,
  `Fname` varchar(25) DEFAULT NULL,
  `Lname` varchar(25) DEFAULT NULL,
  `Usertype` char(10) DEFAULT NULL,
  `Year` year(4) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `Approvedstatus` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Rid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `registration` */

insert  into `registration`(`Rid`,`Fname`,`Lname`,`Usertype`,`Year`,`Username`,`Password`,`Approvedstatus`) values ('1000','Geevar','C','staff',2017,'geevar@gmail.com','geevarc','approved'),('1002','Noushad','C V','staff',2017,'noushad@gmail.com','noushad','approved'),('11002','Sabu','K','staff',2018,'sabu@gmail.com','sabu','approved'),('11003','sabiq','km','student',2018,'sabiq@gmail.com','sabiq','approved'),('11022','anu','vv','student',2018,'anu@gmail.com','anu','approved'),('11023','ammu','mk','student',2018,'ammu@gmail.com','ammu','approved'),('11032','seetha','K','student',2018,'seetha@gmail.com','seetha','approved'),('11033','neenu','K','student',2018,'neenu@gmail.com','neenu','approved'),('11102','mithun','K','staff',2018,'mithu@gmail.com','mithun','approved'),('11122','appu','K','student',2018,'appu@gmail.com','appu','approved'),('12121','sreetha','cv','staff',2018,'sreetha@gmail.com','sreetha','approved'),('MES16MCA17','Haritha','C P','student',2019,'haritha@gmail.com','haritha','approved'),('mes4','Anjana','K P','student',2018,'sumith@gmail.com','anjanasumith','approved');

/*Table structure for table `setdate` */

DROP TABLE IF EXISTS `setdate`;

CREATE TABLE `setdate` (
  `startdate` date NOT NULL,
  `enddate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `setdate` */

insert  into `setdate`(`startdate`,`enddate`) values ('2018-11-25','2018-11-30');

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `Rid` varchar(20) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `Usertype` varchar(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `staff` */

insert  into `staff`(`Rid`,`Username`,`Password`,`Usertype`) values ('11002',NULL,NULL,'coordinator');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
