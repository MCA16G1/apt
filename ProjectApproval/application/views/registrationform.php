<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Student Project Approval</title>
   <!-- core CSS -->
    <link href="<?php echo base_url();?>staffassets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo base_url();?>staffassets/css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->  
  
  
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="banner"  style="background-color:;">
            <div class="container"  style="background-color: ;">
<br>

                
                    <a><h2 style="text-align: center;color: #ff952b;">Student Project Approval</h2></a>
              
            
                <div class="collapse navbar-collapse navbar-right">
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
    <a href="<?php echo base_url() ?>welcome/test"></a>



<center><h2>REGISTRATION</h2></center>
<form method="post" action="<?php echo base_url() ?>welcome/insert">
<center>
   <table class="table table-bordered" style="width: 50%;">
        <tr class="form-group">
         <td>Staff ID/Student Reg No</td>
         <td><input type="text" name="R_id"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td>First Name</td>
         <td><input type="text" name="F_name"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td>Last Name</td>
         <td><input type="text" name="L_name"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td>User Type</td>
         <td>
            <select name="Usertype" class="form-control"style="height: 7vh;">
               <option value="student"class="form-control">STUDENT</option>
               <option value="staff"class="form-control">STAFF</option>
            </select>
        </td>
        </tr>
        <tr>
         <td>Year of Admission</td>
         <td><input type="text" name="Year"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td>Username</td>
         <td><input type="text" placeholder="Staff ID/Student Reg No" name="Email"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td>Password</td>
         <td><input type="password" name="Pword"style="height: 7vh;"class="form-control"style="height: 7vh;"/></td>
        </tr>
        <tr>
         <td></td><td><input type="submit" value="Register"class="btn btn-info"style="width: 45%;height: 7vh"/>
         <input type="button" value="Cancel"class="btn btn-danger"style="width: 45%;height: 7vh"/></td>
        </tr>
 </table>
</center>
</form>
<footer>
         <div id="footer">
            <div class="container">
               <div class="row row-bottom-padded-md">
                  <div class="col-md-12 col-sm-12 col-xs-12 fh5co-footer-link" style="text-align: center;">
                     <h3 style="color: #fff;">About </h3>
                     <p>Student can submit maximum three topics with abstract before due date.
Faculty can access the details of the students and can perform keyword based search to check whether the topic is already exist or not.</p>
                  </div>
                  
                  
               
            </div>
         </div>
      </div>
      </footer>
   <!-- END fh5co-wrapper -->

   <!-- jQuery -->


   <script src="<?php echo base_url();?>homeassets/js/jquery.min.js"></script>
   <!-- jQuery Easing -->
   <script src="<?php echo base_url();?>homeassets/js/jquery.easing.1.3.js"></script>
   <!-- Bootstrap -->
   <script src="<?php echo base_url();?>homeassets/js/bootstrap.min.js"></script>
   <!-- Waypoints -->
   <script src="<?php echo base_url();?>homeassets/js/jquery.waypoints.min.js"></script>
   <script src="<?php echo base_url();?>homeassets/js/sticky.js"></script>

   <!-- Stellar -->
   <script src="<?php echo base_url();?>homeassets/js/jquery.stellar.min.js"></script>
   <!-- Superfish -->
   <script src="<?php echo base_url();?>homeassets/js/hoverIntent.js"></script>
   <script src="<?php echo base_url();?>homeassets/js/superfish.js"></script>
   <!-- Magnific Popup -->
   <script src="<?php echo base_url();?>homeassets/js/jquery.magnific-popup.min.js"></script>
   <script src="<?php echo base_url();?>homeassets/js/magnific-popup-options.js"></script>
   <!-- Date Picker -->
   <script src="<?php echo base_url();?>homeassets/js/bootstrap-datepicker.min.js"></script>
   <!-- CS Select -->
   <script src="<?php echo base_url();?>homeassets/js/classie.js"></script>
   <script src="<?php echo base_url();?>homeassets/js/selectFx.js"></script>
   
   <!-- Main JS -->
   <script src="<?php echo base_url();?>homeassets/js/main.js"></script>

   </body>
</html>
