


  <script src="<?php echo base_url();?>homeassets/js/docraptor-1.0.0.js"></script>
  <script>
    var downloadPDF = function() {
      DocRaptor.createAndDownloadDoc("YOUR_API_KEY_HERE", {
        test: true, // test documents are free, but watermarked
        type: "pdf",
        document_content: document.querySelector('#print').innerHTML, // use this page's HTML
        // document_content: "<h1>Hello world!</h1>",               // or supply HTML directly
        // document_url: "http://example.com/your-page",            // or use a URL
        // javascript: true,                                        // enable JavaScript processing
        // prince_options: {
        //   media: "screen",                                       // use screen styles instead of print styles
        // }
      })
    }
  </script>
 <style>
    @media print {
      #pdf-button {
        display: none;
      }
    }
  </style>
  
  
  <style>
	table
	{
	text-align:left;
	}
	table tr
	{
	text-align:left;
	}
	table tr td
	{
	width:50%;
	text-align:left;
	}
  
  </style>
</head>
<body>

<div id="print">
<div style="width:100%;border: 2px solid;">
	<?php if(is_array($absresult)>0){
	foreach ($absresult as $row) {
	 ?>
  <div>
			<center>
				<h4><u>MES COLLEGE OF ENGINEERING - KUTTIPPURAM</u></h4>
				<h4><u>DEPARTMENT OF COMPUTER APPLICATION</u></h4>
				<h4><u>RLMCA352 - PROJECT</u></h4>
				<h2>PRO FORMA FOR THE APPROVAL OF THE FINAL SEMESTER PROJECT</h2>
				<h6><i>(Note:All entries of the pro forma of approval should be filled up with appropriate and complete information.Incomplete pro forma of approval in any respect will be rejected.)</i></h6>
			</center>
				</div>

				<div>
					<center>
					<table border="1px;"style="width: 100%;border-collapse:collapse;" class="table table-bordered table-striped">
						<tr>
							<td rowspan="2">Project Proposal Number :- </td><td>Academic year :-<?php echo $row->Acdyear ;?></td>

						</tr>
						<tr>
							<td>Year of admission :-<?php echo $row->YearofAdm; ?></td>
						</tr>
						<tr>
							<td>E-Mail :-<?php echo $row->Email ?></td><td>Admission Number :-</td>
						</tr>
						<tr>
							<td rowspan="2">Mobile No :-<?php echo $row->StdMob; ?></td><td>Roll Number :-<?php echo $row->RollNo ?></td>
						</tr>
						<tr>
							<td>Register Number :-<?php echo $row->RegNo; ?></td>
						</tr>

					</table>
					<table style="width: 100%; text-align:left;">
						<tr>
							<td style="width: 50%;">1. Name Of The Student(In BLOACK LETTERS)</td><td >:-<?php echo $row->StudentName; ?></td>
						</tr>
						<tr>
							<td style="width: 50%;">2. Name Of The Organization</td><td>:-</td>
						</tr>
						<tr>
							<td style="width: 50%;">3. Address Of the Organization</td><td>:-</td>
						</tr>
						<tr>
							<td style="width: 50%;">Telephone No :-</td><td>Company E-Mail :-</td>
						</tr>
						<tr>
							<td style="width: 50%;">4. Name Of The External Guide</td><td>:-</td>
						</tr>
						<tr>
							<td style="width: 50%;">Mobile No :-</td><td>E-Mail :-</td>
						</tr>
						<tr>
							<td colspan="2">title of the priject :-<?php echo $row->AbstractFile; ?></td>
						</tr>
												<tr>
							<td colspan="2">Name of the guide(Internal department) :-</td>
					</tr>
					</table>
					<table style="width: 100%;">
						<tr>
							<td style="width: 50%;">Date</td><td>Signature of the Student</td>
						</tr>
						<tr>
							<td style="width: 50%;"><h4>Comments Of The Projrct Guide</h4></td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;">Initial Submission :-</td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;">Approval Status :-<?php echo $row->Status; ?></td><td>Date  Signature Of Guide  HOD</td>
						</tr>
						<tr>
							<td style="width: 50%;">First Review :-</td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;">Second Review :-</td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;"><h4>Comments of the project Coordinator</h4></td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;">Initial Submission :-</td><td></td>
						</tr>
												<tr>
							<td style="width: 50%;">First Review :-</td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;">Second Review :-</td><td></td>
						</tr>
						<tr>
							<td style="width: 50%;"></td><td>Date Signature of Projacet Coordinator</td>
						</tr>

					</table>
					</center>
					<br>

				</div>
			<?php } }?>
				</div>
				<br>
<!-- 				<div style="border: 2px solid;">
					<?php if(isset($selectfile)>0) {?>
						<div>
							
							<?php echo $selectfile ;?>
						</div>
					<?php } ?>
				</div> -->
				
			</div>
  	<center>
<input id="pdf-button" type="button" value="Download PDF" onclick="downloadPDF()" />
</center>
</body>