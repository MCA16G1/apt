				<footer>
			<div id="footer">
				<div class="container">
					<div class="row row-bottom-padded-md">
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>About </h3>
							<p>Student can submit maximum three topics with abstract before due date.
Faculty can access the details of the students and can perform keyword based search to check whether the topic is already exist or not.</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Top Digital Libraries</h3>
							<ul>
								<li><a href="https://ieeexplore.ieee.org/Xplore/home.jsp">IEEE Digital Library</a></li>
								<li><a href="https://link.springer.com/">Springer</a></li>
								<li><a href="https://www.ibiblio.org/">Ibiblio</a></li>
								<li><a href="https://books.google.com/">Google Books</a></li>
								<li><a href="https://openlibrary.org/">Open Library</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Contact</h3>
							<ul>
								<li><a href="#">Dept of MCA, MES College of Engineering</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Details</h3>
							<ul>
								<li><a href="https://ktu.edu.in/eu/acd/viewSyllabus.htm?curriculumId=s2f%2BQViV6a4ygRrpZk5DaTVppwFpznNfbu9On4xM1jk%3D&orgId=cmTvJJh572i4UyLUXRHzH05UiBz2RiIV%2FhckcFn9LWU%3D">syllabus</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						
					
				</div>
			</div>
		</footer>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<script src="<?php echo base_url();?>homeassets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/sticky.js"></script>

	<!-- Stellar -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="<?php echo base_url();?>homeassets/js/hoverIntent.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/superfish.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap-datepicker.min.js"></script>
	<!-- CS Select -->
	<script src="<?php echo base_url();?>homeassets/js/classie.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/selectFx.js"></script>
	
	<!-- Main JS -->
	<script src="<?php echo base_url();?>homeassets/js/main.js"></script>

	</body>
</html>

