
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Project Approval &mdash; 100% Free Fully Responsive HTML5 Template by FREEHTML5.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url();?>homeassets/favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/superfish.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/magnific-popup.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/bootstrap-datepicker.min.css">
	<!-- CS Select -->
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/cs-select.css">
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/cs-skin-border.css">
	
	<link rel="stylesheet" href="<?php echo base_url();?>homeassets/css/style.css">
	<script src="<?php echo base_url();?>homeassets/js/jquery.js"></script>
			<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">

		<header id="fh5co-header-section" class="sticky-banner">
			<div class="container">
				<div class="nav-header">
					<!--<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>-->
					<center><h1 id="fh5co-logo"><a href="#">Approval of Project Topics</a></h1></center>
					<!-- START #fh5co-menu-wrap -->
					<nav id="fh5co-menu-wrap" role="navigation">
						<ul class="sf-menu" id="fh5co-primary-menu">
							<li><a href="<?php echo base_url();?>coordinatorhome">Home</a></li>
							<!-- <li><a href="<?php echo base_url();?>coordinatorprofile">Profile</a></li> -->
							<li>
								<a href="#" class="fh5co-sub-ddown">Pending List</a>
								<ul class="fh5co-sub-menu">
									
									<li><a href="<?php echo base_url();?>co_pending_stud">Student</a></li>
								</ul>
							</li>
							<li>
								<a href="#" class="fh5co-sub-ddown">Registered List</a>
								<ul class="fh5co-sub-menu">
									<li><a href="<?php echo base_url();?>co_staff_details">Set Guide</a></li>
									<li><a href="<?php echo base_url();?>co_guide">Guide List</a></li>
									<li><a href="<?php echo base_url();?>co_registered_stud">Student List</a></li>

								</ul>
							</li>
							<li>
								<a href="#" class="fh5co-sub-ddown">Activities</a>
								<ul class="fh5co-sub-menu">
									
									<li><a href="<?php echo base_url();?>co_guideallocation">Student Allocation</a></li>
									<li class="active"><a href="<?php echo base_url();?>co_studentdetails">Search Abstract</a></li>
									<li><a href="<?php echo base_url();?>co_notification">Notification</a></li>
								</ul>
							</li>
						    
						    <li><a href="<?php echo base_url();?>Welcome/Logout">Logout</a></li>
							
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<!-- end:header-top -->
	
		<div>
			<?php if(isset($per)) 
{
?>
					<div>
						<br>
						<center>
								<div style=" overflow: auto;width: 50; ">
				<ul>
				<h3>Select Abstract</h3>	
<select id="dropdownList" class="form-control" style="width: 50%;" name="gid">
							<option value="#">--Select--</option>
							<?php foreach ($file as $row) { ?>
							 <option value="<?php echo $row['file_name']; ?>"><?php echo $row['file_name']; ?>-<?php echo $row['percentage']; ?></option>
							<?php } ?>
						</select>


				<!-- 		<li style="float: right;" value="">
						<input type="button" id="link1" style="text-align: right;" value="<?php echo $row['file_name']; ?>"><?php echo $row['file_name']; ?> - <?php echo $row['percentage']; ?>
						
					</a>
					</li> -->

<!--<li style="float: right;"><input type="hidden" id="link" value="<?php echo $row['file_name']; ?>">
						<a href="#" id="link1" style="text-align: right;" value=""><?php echo $row['file_name']; ?> - <?php echo $row['percentage']; ?>
						
					</a>
					</li> -->
					
				</ul>
			</div>
			</center>
						<br>
						<h1 style="text-align: center;">Search Result</h1>
			<div class="col-md-6 col-sm-6 col-xs-12" style="height: 70vh; overflow: auto; border:2px solid;">
				<h2>Search File</h2>

				<h3><?php echo $selectfile ;?></h3>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12" style="height: 70vh; overflow: auto;border:2px solid;" id="ciudad">

				<!-- <h2><?php echo $sfile;?></h2> -->


			</div>
	
			<br>
		<br>
		<br>
		<br>
		<br>
		<br><br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<?php } else{?>


		<div>
			<?php if(isset($error)){ ?>
			<?php echo "<h2>".$error."</h2>"; ?>
		<?php } ?>
		<?php if(isset($message)){
  echo "<h2>".$message."</h2>";
} ?>
			<br><br><center><h1>Search</h1></center>
			<table class="table table-bordered table-striped">
				<tr>
				<th>Student ID</th>
				<th>Student Name</th>
				<th>Abstract<h4 style="text-align: center;">Comments and details</h4></th>
				</tr>
					<?php if(isset($std_details)){
					foreach ($std_details as $srow) {
					 	
					  ?>
				<tr>
					<td><?php echo $srow->Rid; ?></td>
					<td><?php echo $srow->Fname; ?></td>
				
				 <?php if ($Searchdata) {
				 	?>
				 	<td>
	<?php
				 	foreach ($Searchdata as $row) 
				 	{
				 		$sid="0";
				 		$sid="$row->RegNo";
				 		if($sid==$srow->Rid)
				 		{?>
				 		<table style="width: 100%"><tr><td style="width: 40%"><a href="<?php echo base_url().'welcome/co_searchab/'.$row->AbstractFile?>">
					<?php echo $row->AbstractFile ;?></a></td><td style="width: 60%;"><textarea name="comments" readonly><?php echo $row->guidecomments ;?></textarea>
					<a href="<?php echo base_url().'welcome/statusapproved/'.$row->AbstractFile.'/'.$srow->Rid?>" class="btn">Approved</a>
					<a href="<?php echo base_url().'welcome/statusrejected/'.$row->AbstractFile?>" class="btn">Rejected</a></td></tr></table>
					<br>
				<?php }} ;?>
				<!-- <a href="<?php echo base_url().'welcome/co_set/'.$row->Rid?>" class="btn btn-info">Set a coordinator</a> -->
				
				</td>
			<?php } else 
			{
				echo "No File Found";
			} ?>
				</tr>
				<?php }} ?>
			</table>
		
		</div>
<?php } ?>
			</div>
			<?php if(isset($absmessage)){echo "<h2>".$absmessage."</h2>";} ?>

 <?php 
					if ($this->session->flashdata('error')) {?>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        	<center>
          <h3 style="color:#384977;"> <?php echo $this->session->flashdata('error'); ?></h3></center>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php } ?>


		
		
				<footer>
			<div id="footer">
				<div class="container">
					<div class="row row-bottom-padded-md">
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>About </h3>
							<p>Student can submit maximum three topics with abstract before due date.
Faculty can access the details of the students and can perform keyword based search to check whether the topic is already exist or not.</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Top Digital Libraries</h3>
							<ul>
								<li><a href="https://ieeexplore.ieee.org/Xplore/home.jsp">IEEE Digital Library</a></li>
								<li><a href="https://link.springer.com/">Springer</a></li>
								<li><a href="https://www.ibiblio.org/">Ibiblio</a></li>
								<li><a href="https://books.google.com/">Google Books</a></li>
								<li><a href="https://openlibrary.org/">Open Library</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Contact</h3>
							<ul>
								<li><a href="#">Dept of MCA, MES College of Engineering</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 fh5co-footer-link">
							<h3>Details</h3>
							<ul>
								<li><a href="https://ktu.edu.in/eu/acd/viewSyllabus.htm?curriculumId=s2f%2BQViV6a4ygRrpZk5DaTVppwFpznNfbu9On4xM1jk%3D&orgId=cmTvJJh572i4UyLUXRHzH05UiBz2RiIV%2FhckcFn9LWU%3D">syllabus</a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						
					
				</div>
			</div>
		</footer>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>

	<script src="<?php echo base_url();?>homeassets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/sticky.js"></script>

	<!-- Stellar -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="<?php echo base_url();?>homeassets/js/hoverIntent.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/superfish.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url();?>homeassets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="<?php echo base_url();?>homeassets/js/bootstrap-datepicker.min.js"></script>
	<!-- CS Select -->
	<script src="<?php echo base_url();?>homeassets/js/classie.js"></script>
	<script src="<?php echo base_url();?>homeassets/js/selectFx.js"></script>
	
	<!-- Main JS -->
	<script src="<?php echo base_url();?>homeassets/js/main.js"></script>






	</body>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
	<?php 
		$this->load->helper('html');
	 ?>
	 <script>
	 	$(document).ready(function()
	 	{
	 		$('#dropdownList').on('change',function()
	 		{
	 			var value=$("#dropdownList").val();

	 			$.ajax(
	 			{
	 				url:"<?php echo base_url(); ?>welcome/select_abs",
	 				async:false,
	 				type:"POST",
	 				data:"file="+value,
	 				datatype:"html",

	 				success: function(data)
	 							{
	 								$('#ciudad').html(data);
	 							}
	 			}

	 				);
	 			
	 		});
	 	});
	 </script>

	</body>
</html>

