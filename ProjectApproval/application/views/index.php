<!DOCTYPE html>
<html lang="en">
  <head>
    <title>mainhome</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/animate.css">
    
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainhomeassets/css/style.css">
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar" data-aos="fade-down" data-aos-delay="500">
      <div class="container">
       
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="#" class="nav-link">Home</a></li>
             <li class="nav-item"><a href="#" class="nav-link">About</a></li>
            <li class="nav-item"><a href="<?php echo base_url();?>adminlogin" class="nav-link">Admin login</a></li>
            <li class="nav-item"><a href="<?php echo base_url();?>mainlogin" class="nav-link">login</a></li>
            <!-- <li class="nav-item"><a href="#" class="nav-link">Blog</a></li> -->
            <li class="nav-item"><a href="#" class="nav-link">Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <section class="ftco-cover overlay" style="background-image: url('<?php echo base_url();?>mainhomeassets/images/cubeblue.jpg');" id="section-home" data-aos="fade"  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center ftco-vh-100">
          <div class="col-md-8 text-center">
            <!-- <h1 class="ftco-heading mb-4" data-aos="fade-up" data-aos-delay="500"><blockquote>APT</blockquote></h1> -->
            <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>mainhomeassets/images/logo1.png" style="height:250px;width:250px;"></a>
            <h4 class="ftco-heading mb-4" data-aos="fade-up" data-aos-delay="500"><i>Approval of Project Topics</i></h4>
            <!-- <h2 class="h5 ftco-subheading mb-5" data-aos="fade-up"  data-aos-delay="600">A free template for Law Firm Websites by <a href="https://colorlib.com/" target="_blank">Colorlib</a> For busy business professionals Lorem ipsum dolor sit amet consectetur adipisicing elit.</h2> -->
            
          </div>
        </div>
      </div>
    </section>

  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.stellar.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/aos.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/jquery.animateNumber.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/google-map.js"></script>
  <script src="<?php echo base_url();?>mainhomeassets/js/main.js"></script>
    
  </body>
</html>