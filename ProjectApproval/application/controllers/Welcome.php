<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}
	public function home()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
    	$this->load->model('my_model');
    	$result=$this->my_model->notificationshow($rid);
    	$count=count($result);
    	//echo $count;
    	$data['count']=$count;
    	$this->load->model('my_model');
    	$data['title']=$this->my_model->not_message($rid);
		

		$this->load->model('my_model');
		$data['notification']=$this->my_model->std_notification();
		$this->load->view('home',$data);
		}
		else
		{
		$this->load->view('index');
		}
	}
	public function mainlogin()
	{
		$this->load->view('mainlogin');
	}
    public function approval_status()
	{
		if($this->session->userdata('Rid'))
		{
			$rid=$this->session->userdata('Rid');
			$this->load->model('my_model');
			$data['result']=$this->my_model->Approvedstatus($rid);

		$this->load->view('approval_status',$data);
	}
	else
		{
			$this->load->view('index');
		}
	}
	public function review_status()
	{
		$this->load->view('review_status');
	}
	public function profile()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
		echo $rid;
		$this->load->model('my_model');
		$data["pro_data"]=$this->my_model->pro_data($rid);
		$this->load->view('profile',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function my_documents()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
		$this->load->model('my_model');
		$data['file']=$this->my_model->selectabstract($rid);
		$this->load->view('my_documents',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function abstract_submission()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
		$this->load->model('my_model');
		$data['pro_data']=$this->my_model->pro_data($rid);
		$this->load->view('abstract_submission',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function notification()
	{
		if($this->session->userdata('Rid'))
		{
			$this->load->view('notification');
		}
		else
		{
			$this->load->view('index');
		}
		
	}
	public function change_password()
	{
		if($this->session->userdata('Rid'))
		{
			$this->load->view('change_password');
		}
		else
		{
			$this->load->view('index');
		}
		
	}
	public function guidehome()
	{
		if($this->session->userdata('Rid'))
		{
		$this->load->model('staff_model');
		$data['notification']=$this->staff_model->gui_notification();
		$this->load->view('guidehome',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function registrationform()
	{

		$this->load->view('registrationform');
	}
	public function studform()
	{
		$this->load->view('studform');
	}
	public function printabs()
	{
		if($this->session->userdata('Rid'))
		{
			$absfile=$this->uri->segment(3);
		$fl='./uploads/'.$absfile;
		$rid=$this->session->userdata('Rid');
		$this->load->model('my_model');
		$data['absresult']=$this->my_model->absresult($absfile,$rid);

		$this->load->model('staff_model');

                $content = $this->staff_model-> read_file_docx($fl);
                if ($content !== false) {

                    $rfile = nl2br($content);
                    
                    $data['selectfile'] = $rfile;
                    //echo $rfile;
                    //$data1['selectfile2']='';
                }
		$this->load->view('printabs',$data);
		}
		else
		{
			$this->load->view('index');
		}
		
	}
	public function guideprofile()
	{
		$this->load->view('guideprofile');
	}

	public function download()
	{
		if($this->session->userdata('Rid'))
		{
		$absfile=$this->uri->segment(3);
		//$fl='./uploads/'.$absfile;
		$rid=$this->session->userdata('Rid');

		$this->load->model('my_model');
		$data['absresult']=$this->my_model->absresult($absfile,$rid);

		/*$this->load->model('staff_model');

                $content = $this->staff_model-> read_file_docx($fl);
                if ($content !== false) {

                    $rfile = nl2br($content);
                    
                    $data['selectfile'] = $rfile;
                    //echo $rfile;
                    //$data1['selectfile2']='';
                }*/
		$this->load->view('download',$data);
		}
		else
		{
			$this->load->view('index');
		}

		
	}
	public function staff_profile()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
		$this->load->model('my_model');
		$data["pro_data"]=$this->my_model->pro_data($rid);
		$this->load->view('staff_profile',$data);
		}
		else
		{
			$this->load->view('index');
		} 


	}
	public function studentdetails()
	{
		if($this->session->userdata('Rid'))
		{
			$this->load->view('studentdetails');
		}
		else
		{
			$this->load->view('index');
		}

		
	}
	public function guidenotification()
	{
		if($this->session->userdata('Rid'))
		{
		$rid=$this->session->userdata('Rid');
		$this->load->model('staff_model');
		$data['std_data']=$this->staff_model->std_data($rid);
		$this->load->view('guidenotification',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	
	public function staff_stud_details()
	{
				if($this->session->userdata('Rid'))
		{
		$this->load->model('my_model');
		$data["fetch_data"] = $this->my_model->fetch_registered_data();
		$this->load->view('staff_stud_details',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function coordinatorhome()
	{
		if($this->session->userdata('Rid'))
		{
			$this->load->view('coordinatorhome');
		}
		else
		{
			$this->load->view('index');
		}
		
	}
	public function coordinatorprofile()
	{
		$this->load->view('coordinatorprofile');
	}
	public function co_guideallocation()
	{
		if($this->session->userdata('Rid'))
		{
		$this->load->model('staff_model');
		$result["lining"]=$this->staff_model->guidedata();
		$this->load->view('co_guideallocation',$result);
		return $result;
		}
		else
		{
			$this->load->view('index');
		}

	}
	function ajax_call()
	{
		$gid=$_POST['guide'];
		$this->load->model('staff_model');
		$data=$this->staff_model->std_guide_data();
		$guide=$this->staff_model->guide_data($gid);


		echo "<table class='table table-bordered table-striped'>";
		echo "<tr>";
		echo "<td>Check Data</td>";
		echo "<td>Student Id</td>";
		echo "<td>Student Name</td>";
		echo "<td>Guide Id</td>";
		echo "<td>Guide Name</td>";
		echo "</tr>";
if ($data) {

		foreach($data as $row)
		{
			echo "<tr>";
			echo "<td><input type='checkbox' name='id[]' value='$row->Rid'></td>";
			echo "<td>".$row->Rid."</td>";
			echo "<td>".$row->Fname."</td>";

			foreach($guide as $row)
			{
				echo "<td>".$row->Rid."</td>";
				echo "<td>".$row->Fname."</td>";
				

			}
			echo "</tr>";
		}
	}
		echo "</table>";

	}
	public function co_studentdetails()
	{
		if($this->session->userdata('Rid'))
		{
					//$rid=$this->session->userdata('Rid');
    	$this->load->model('staff_model');
    	$data['std_details']=$this->staff_model->search_abstract();

    	
    	$this->load->model('staff_model');
    	$data['Searchdata']=$this->staff_model->abstract__std();
		$this->load->view('co_studentdetails',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function co_staff_details()
	{
		if($this->session->userdata('Rid'))
		{
			$this->load->model('staff_model');
			$data["fetch_data"] = $this->staff_model->fetch_registered_data();
			$this->load->view('co_staff_details',$data);
		}
		else
		{
			$this->load->view('index');
		}

	}
	public function co_guide()
	{

		$this->load->model('staff_model');
		$data["fetch_data"] = $this->staff_model->fetch_guide_data();
		$this->load->view('co_guide',$data);
	}
	public function co_notification()
	{
		$this->load->view('co_notification');
	}
	public function staff_changepassword()
	{
		$this->load->view('staff_changepassword');
	}
	public function adminlogin()
	{
		$this->load->view('adminlogin');
	}
	public function adminhome()
	{
		$this->load->view('adminhome');
	}
	public function co_pending_stud()
	{
		$this->load->model('my_model');
		$data["fetch_data"] = $this->my_model->fetch_data();
		$this->load->view('co_pending_stud',$data);
	}
	public function co_registered_stud()
	{
		$this->load->model('staff_model');
		$data["fetch_data"] = $this->staff_model->registered_data();
		$this->load->view('co_registered_stud',$data);
	}
	public function admin_pending_staff()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_data();
		$this->load->view('admin_pending_staff',$data);

	}
	public function admin_registered_staff()
	{
		$this->load->model('admin_model');
		$data["fetch_data"] = $this->admin_model->fetch_registered_data();
		$this->load->view('admin_registered_staff',$data);
	}
	public function admin_set_coordinator()
	{
		$this->load->model('admin_model');
		$test=$this->admin_model->testingcood();
		$res= $this->admin_model->fetch_registered_data();

		$data['testing']=$test;
		$data["fetch_data"] =$res;
		
		$this->load->view('admin_set_coordinator',$data);
	}

	public function deletecood()
	{
		$this->load->model('admin_model');
		$this->admin_model->deletecood();
				$this->session->set_flashdata('error',' Coordinator Deleted Successfully');
		redirect(base_url().'admin_set_coordinator',$message);
	}
	public function staffhome()
	{
		/*if($this->session->userdata('Rid'))
		{
		$this->load->view('home');
		}
		else
		{
		$this->load->view('index');
		}*/
		$rid=$this->session->userdata('Rid');
		$this->load->model('staff_model');
		$data["lining"]=$this->staff_model->notification($rid);
		$data["abc"]=$this->staff_model->guidenotification($rid);
		$this->load->view('staffhome',$data);
	}
	public function log_validation()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('pass','password','required');
		if($this->form_validation->run())
		{
			$this->load->model('my_model');
			$user=$this->input->post('username');
			$pass=$this->input->post('pass');
			$userRow = $this->my_model->can_login($user,$pass);
			if($userRow)
			{
				$userType = $userRow->Usertype;
				$approved = $userRow->Approvedstatus;
			}
			else
			{
				$userType="";
				$approved = "";
			}
			if($userType == "student" && $approved == "approved")
			{
				$rid=$this->my_model->getrid($user);
				if($this->session->set_userdata('Rid',$rid));
				{
				//$rid=$this->session->userdata('Rid');
				
				
				redirect(base_url().'welcome/home');
			}
			}

			else if($userType == "staff" && $approved == "approved")
			{
				$rid=$this->my_model->getrid($user);
				$this->session->set_userdata('Rid',$rid);
				$rid=$this->session->userdata('Rid');
				redirect(base_url().'welcome/staffhome');
			}
			else if(($userType == "staff" || $userType == "student") && $approved != "approved")
			{
				$this->session->set_flashdata('error','Not Approved');
				redirect(base_url().'welcome/mainlogin');
			}
			else
			{
				//redirect(base_url().'welcome/index');
				$this->session->set_flashdata('error','Invalid Username Or Password');
				redirect(base_url().'welcome/mainlogin');
			}
		}
		else
		{
			$this->index();
		}
	}
	

	public function insert()
	{
		
		$this->form_validation->set_rules('R_id','Registration_id','required');
		$this->form_validation->set_rules('F_name','F_name','required');
		$this->form_validation->set_rules('L_name','L_name','required');
		$this->form_validation->set_rules('Usertype','usertype','required');
		$this->form_validation->set_rules('Year','year','required');
		$this->form_validation->set_rules('Email','Email','required');
		$this->form_validation->set_rules('Pword','password','required');


		

		if($this->form_validation->run())
		{
			$this->load->model('my_model');
			$data=array('Rid' => $this->input->post('R_id'),
                        'Fname' => $this->input->post('F_name'),
                        'Lname' => $this->input->post('L_name'),
                        'Usertype' => $this->input->post('Usertype'),
                        'Year' => $this->input->post('Year'),
                        'Username' => $this->input->post('Email'),
                        'Password' => $this->input->post('Pword'),
                        'Approvedstatus' => 'pending');

			$this->my_model->insert($data);
				$this->session->set_flashdata('error','Registration successful');
			redirect(base_url().'mainlogin',$message);		
		}

		else
		{
			$this->index();
		}
	}
	public function abstract_insert()
	{
		$sdate="";
		$edate="";
		$this->load->model('staff_model');
		$date=$this->staff_model->selectdate();
		foreach ($date as $value) {
			$sdate=$value->startdate;
			$edate=$value->enddate;
		}


		$this->form_validation->set_rules('acdy','acdyear','required');
		$this->form_validation->set_rules('admyr','admyear','required');
		$this->form_validation->set_rules('admno','admno','required');
		$this->form_validation->set_rules('rno','rollno','required');
		$this->form_validation->set_rules('regno','regnumber','required');
		$this->form_validation->set_rules('stdemail','studentemail','required');
		$this->form_validation->set_rules('stdno','mobilenumber','required');
		$this->form_validation->set_rules('stdname','stuname','required');
		$this->form_validation->set_rules('protitle','protitle','required');
		$this->form_validation->set_rules('date','date','required');
		$this->form_validation->set_rules('absno','absno','required');

$absdate= $this->input->post('date');

if($absdate>$sdate and $absdate<$edate)
{


		if($this->form_validation->run()==true)
		{

			 $config['upload_path']= './uploads/';
             $config['allowed_types'] = '*';
             $config['max_size'] = 10000;


                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        
                    //$this->form_validation->set_error_delimiters('<p class="error">', '</p>');

                    $data = array('error' => $this->upload->display_errors());

                    $this->load->view('Abstract_Submission', $data);
                }
                else
                {
                    $updata = array('AbstractFile' => $this->upload->data('file_name'));

                    //$this->load->view('success', $data);
                


			$this->load->model('my_model');
			$indata=array(
                       'Acdyear' => $this->input->post('acdy'),
                        'YearofAdm' => $this->input->post('admyr'),
                        'AdmNo' => $this->input->post('admno'),
                        'RollNo' => $this->input->post('rno'),
                        'RegNo' => $this->input->post('regno'),
                        'Email' => $this->input->post('stdemail'),
                        'StdMob' => $this->input->post('stdno'),
                        'StudentName' => $this->input->post('stdname'),
                        'ProjectName' => $this->input->post('protitle'),
                        
                        'Date' => $this->input->post('date'),
                        'AbstractNo' => $this->input->post('absno'),
                       
                        
                        'Status' => 'pending');

			$res = array_merge($updata,$indata); 
			$this->my_model->abstract_insert($res);

			$this->session->set_flashdata('error','Abstract inserted');
			redirect(base_url().'welcome/Abstract_Submission');	
			}	
		}

		else
		{
			$this->load->view('Abstract_Submission');
		}
	}
	else
	{
		$this->session->set_flashdata('error','This date not allowed');
		$this->load->view('Abstract_Submission');
		/*$error="date out of range";
		$this->load->view('Abstract_Submission',$error);*/
	}
	}
	public function notify_insert()
	{
		$this->form_validation->set_rules('title','Title','required');
		
		$this->form_validation->set_rules('msg','message','required');
		

		if($this->form_validation->run())
		{
			$this->load->model('staff_model');
			$cod1=0;
			$cod2=0;
			if($this->input->post('cod1')!=null)
			{
				$cod1=1;
			}
			if($this->input->post('cod2')!=null)
			{
				$cod2=1;

			}
			if($cod1!=null || $cod2!=null)
			{
			$data=array('title'=>$this->input->post('title'),'message'=>$this->input->post('msg'),'guide'=>$cod1,'student'=>$cod2);
			$this->staff_model->notify_insert($data);
			$this->session->set_flashdata('error','Message sent');
			redirect(base_url().'welcome/co_notification');		
			}
			else
			{
				$this->session->set_flashdata('msg','Please click checkbox');
				$this->data['msg']=$this->session->flashdata('msg');
				$this->load->view('co_notification',$this->data);
			}
			
		}	
		
	}

	public function admin_validation()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('pass','password','required');
		if($this->form_validation->run())
		{
			$aduser=$this->input->post('username');
			$adpass=$this->input->post('pass');
			$this->load->model('admin_model');

			if($this->admin_model->can_login($aduser,$adpass))
			{
				$this->load->library('session');
				$session_data=array('username' => $aduser);
				$this->session->set_userdata($session_data);
				redirect(base_url().'welcome/adminhome');
			}

			else
			{
				redirect(base_url().'welcome/adminlogin');
				$this->session->set_flashdata('error','Invalid Username Or Password');
				redirect(base_url().'welcome/adminlogin');
			}
		}
		
		else
		{
						$this->index();

			
		}

    }


public function staff_validation()
	{

$rid=$this->session->userdata('Rid');
	
			$this->load->model('staff_model');
			if($this->staff_model->can_login($rid) == "coordinator")
			{
				redirect(base_url().'welcome/coordinatorhome');
			}

			else if($this->staff_model->can_login($rid) == "guide")
			{

				redirect(base_url().'welcome/guidehome');
			}

			else
			{
				$msg['message']="You Not Permition";
				redirect(base_url().'welcome/staffhome',$msg);
				
				
			}
		
	}
 
public function update()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->update($data);
	redirect('admin_pending_staff');
}
public function reject()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->reject($data);
	redirect('admin_pending_staff');
}
/*public function co_set()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->co_set($data);
	redirect('admin_set_coordinator');
}*/
public function stu_update()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->update($data);
	redirect('co_pending_stud');
}
public function stu_reject()
{
	$data=$this->uri->segment(3);
	$this->load->model('admin_model');
	$this->admin_model->reject($data);
	redirect('co_pending_stud');
}
public function co_set()
{
	$data['segment']=$this->uri->segment(3);
	$this->load->view('admin_set_coordinator',$data);
}
public function guide_set()
{
	$data['segment']=$this->uri->segment(3);
	$this->load->view('co_staff_details',$data);
}
public function co_insert()
{
	$this->form_validation->set_rules('R_id','Registration_ID','required');
	$this->form_validation->set_rules('type','Type','required|is_unique[staff.Usertype]',array('is_unique'=> 'Coordinator Already Exist'));
	if($this->form_validation->run()=="true")
	{
		$this->load->model('admin_model');
		$data=array('Rid'=>$this->input->post('R_id'),
					'Usertype'=>$this->input->post('type'));
		$this->admin_model->testing_insert($data);
		redirect(base_url().'admin_set_coordinator');	}
	else
	{
		$data['segment']=$this->input->post('id');
		$this->load->view('admin_set_coordinator',$data);
	}
}
public function guide_insert()
{
	$this->form_validation->set_rules('R_id','Registration_ID','required');
	$this->form_validation->set_rules('type','Type','required');
	if($this->form_validation->run()=="true")
	{
		$this->load->model('staff_model');
		$data=array('Rid'=>$this->input->post('R_id'),
					'Usertype'=>$this->input->post('type'));
		$this->staff_model->guide_insert($data);
		redirect(base_url().'co_staff_details');
	}
	else
	{
		$data['segment']=$this->input->post('id');
		$this->load->view('co_staff_details',$data);
	}
	
}
public function delete($id)
	{
		$this->load->model("admin_model");
		$this->admin_model->delete($id);
		redirect(base_url().'admin_registered_staff');
	}
public function stud_delete($id)
	{
		$this->load->model("my_model");
		$this->my_model->stud_delete($id);
		//$this->load->view('co_registered_stud');
		redirect(base_url().'co_registered_stud');
	}
public function guide_delete($id)
	{
		$this->load->model("staff_model");
		$this->staff_model->guide_delete($id);
		redirect(base_url().'co_guide');
	}

public function pass_validation()
	{
		$this->form_validation->set_rules('cu_pass','current_password','required');
		$this->form_validation->set_rules('new_pass','new_password','required');
		//$this->form_validation->set_rules('c_pass','confirm_password','required');
		if($this->form_validation->run())
		{
			
			$rid=$this->session->userdata('Rid');
			$cupass=$this->input->post('cu_pass');

			$this->load->model('my_model');
			$data=$this->input->post('new_pass');
			if($this->my_model->can_change($rid,$cupass,$data))
			{
				
				/*$newpass=$this->input->post('new_pass');
				$this->load->model('my_model');
				$this->my_model->pass_insert($newpass);*/
			$this->session->set_flashdata('error','Password changed');
				redirect(base_url().'change_password');


				/*$this->load->library('session');
				$session_data=array('username' => $user);
				$this->session->set_userdata($session_data);*/
				//echo "string";
				//redirect(base_url().'welcome/home');
			}
			else 
			{
				echo "sting33e333";
			}
		}
		else
		{
			echo "string3333";
		}
}
public function staff_pass_validation()
	{
		$this->form_validation->set_rules('cu_pass','current_password','required');
		$this->form_validation->set_rules('new_pass','new_password','required');
		
		if($this->form_validation->run())
		{
			
			$rid=$this->session->userdata('Rid');
			$cupass=$this->input->post('cu_pass');

			$this->load->model('my_model');
			$data=$this->input->post('new_pass');
			if($this->my_model->staff_can_change($rid,$cupass,$data))
			{	
				$this->session->set_flashdata('error','Password changed');
				redirect(base_url().'staff_changepassword');
			}
			else 
			{
				
			}
		}
		else
		{
			
		}
}
		public function setstudent()
		{
			$row="0";
			$this->form_validation->set_rules('id[]','id','required');
			if($this->form_validation->run())
			{
				$gid=$this->input->post('gid');
				$id=array($this->input->post('id'));
				foreach($id as $row)
				{

				}
				$this->load->model('staff_model');
				$data=$this->staff_model->selectstddata($row);

				foreach($data as $row)
				{
					/*echo $row->Rid;
					echo $row->Fname;
					echo "<br>";*/
				}

				$this->load->model('staff_model');
				$gdata=$this->staff_model->selectguidedata($gid);
				foreach($gdata as $grow)
				{
					/*echo $grow->Rid;
					echo $grow->Fname;
					echo "<br>";*/
				}
				$this->load->model('staff_model');
				$this->staff_model->insertallocation($data,$gdata);
				redirect(base_url().'co_guideallocation');
			}
		}

		function do_upload()
		{
			$this->load->library('upload');
			$files=$_FILES;
			$cpt=count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++)
			{
				$_FILES['userfile']['name']       =   $files['userfile']['name'][$i]; 
				$_FILES['userfile']['type']       =   $files['userfile']['type'][$i]; 
				$_FILES['userfile']['tmp_name']   =   $files['userfile']['tmp_name'][$i]; 
				$_FILES['userfile']['error']      =   $files['userfile']['error'][$i]; 
				$_FILES['userfile']['size']       =   $files['userfile']['size'][$i]; 
                
                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();

                $upload_data  =   $this->upload->data();
                $file_name    =   $upload_data['file_name'];
                $file_type    =   $upload_data['file_type'];
                $file_size    =   $upload_data['file_size'];

                $data['getfiledata_file_name']  =  $file_name;       //output control
                $data['getfiledata_file_type']  =  $file_type;
                $data['getfiledata_file_size']  =  $file_size;

                $this->load->model('my_model');                   //insert data for current file
                $this->my_model->insertNotice($data);

                $this->load->view('Abstract_Submission',$data);   //create a view containing just the text "upload successfully"

			}
		}

		private function set_upload_options()
		{
			//upload a doc option
			$config                   =   array();
			$config['upload_path']    =   './fileselif/';
			$config['allowed_types']  =   'txt|doc|docx';
			$config['max_size']       =   '10000';
			$config['overwrite']      =   FALSE;

			return $config;


		}
		public function Logout()
    	{
       		$sess_array = array('Rid' => '');
			$this->session->unset_userdata('Rid', $sess_array);
			$data['message_display'] = 'Successfully Logout';
			$this->load->view('index', $data);
    	}
		public function test()
		{
			         echo "<div class='alert alert-success alert-dismissible' role='alert'>
<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
<strong>Congratulations!</strong> You Message successfully send
</div>";
		}


		public function guide_set_notification()
		{
			$gid=$this->session->userdata('Rid');
			$this->form_validation->set_rules('title','Title','required');
			$this->form_validation->set_rules('message','Message','required');
			$this->form_validation->set_rules('id[]','StudentId','required');


			$this->load->model('staff_model');
			$title=$this->input->post('title');
			$id=$this->input->post('id');


			$data=$this->input->post('message');
					/*foreach ($id as $row) {
						echo $row;					}*/

			$this->staff_model->isert_not($data,$id,$title,$gid);
			redirect(base_url().'guidenotification');

		}

    public function search()
    {
    	$rid=$this->session->userdata('Rid');
    	$this->load->model('staff_model');
    	$data['std_details']=$this->staff_model->searchstddata($rid);

    	
    	$this->load->model('staff_model');
    	$data['Searchdata']=$this->staff_model->searchfile($rid);
    	$this->load->view('search',$data);

    }





public function searchab() {
    $filename = $this->uri->segment(3);
    $fl = './uploads/'.$filename;
    $this->load->model('staff_model');
    $lines=$this->staff_model->read_file_docx($fl);
   

    //$lines = file_get_contents($fl);
    $arr1 = explode(" ", $lines);
    $cta1 = count($arr1);
   
    $dir = "./uploads/";
    $files = array_diff(scandir($dir), array('.', '..'));

    $p = false;

    foreach($files as $file) {
    	$fl2 = './uploads/'.$file;

	$this->load->model('staff_model');
    $lines2=$this->staff_model->read_file_docx($fl2);
        /*$lines2 = file_get_contents("./uploads/".$file);*/

        $arr2 = explode(" ", $lines2);
        if ($file != $filename) {

            $result = array_intersect($arr2, $arr1);

            $tot = count($result);
            

            foreach($result as $res) {
/*
                if ($res == 'this'
                    or $res == 'is') {

                    $tot -= 1;

                }*/

            }

            $p = ($tot / $cta1) * 100;
            if ($p >= 50) {

                $this->load->model('staff_model');

                $content = $this->staff_model->read_file_docx($fl);
                if ($content !== false) {

                    $rfile = nl2br($content);
                    $data['selectfile'] = $rfile;
                    //echo $rfile;
                    //$data1['selectfile2']='';
                }

                $data['per'][] = floor($p.
                    "%");
                //$data['file'][] = $file;
                $data['file'][] = array(
                	'file_name' => $file,
                	'percentage' => floor($p.
                    "%")
                );
                //$this->load->view('search',$data);

            }

        }
    }

    if ($p < 50) {
        $data['absmessage'] = "Not found";

    }
    $this->load->view('search', $data);
}







public function co_searchab() {
       $filename = $this->uri->segment(3);
    $fl = './uploads/'.$filename;
    $this->load->model('staff_model');
    $lines=$this->staff_model->read_file_docx($fl);
   

    //$lines = file_get_contents($fl);
    $arr1 = explode(" ", $lines);
    $cta1 = count($arr1);
   
    $dir = "./uploads/";
    $files = array_diff(scandir($dir), array('.', '..'));

    $p = false;

    foreach($files as $file) {
    	$fl2 = './uploads/'.$file;

	$this->load->model('staff_model');
    $lines2=$this->staff_model->read_file_docx($fl2);
        /*$lines2 = file_get_contents("./uploads/".$file);*/

        $arr2 = explode(" ", $lines2);
        if ($file != $filename) {

            $result = array_intersect($arr2, $arr1);

            $tot = count($result);
            

            foreach($result as $res) {
/*
                if ($res == 'this'
                    or $res == 'is') {

                    $tot -= 1;

                }*/

            }

            $p = ($tot / $cta1) * 100;
            if ($p >= 50) {

                $this->load->model('staff_model');

                $content = $this->staff_model->read_file_docx($fl);
                if ($content !== false) {

                    $rfile = nl2br($content);
                    $data['selectfile'] = $rfile;
                    //echo $rfile;
                    //$data1['selectfile2']='';
                }

                $data['per'][] = floor($p.
                    "%");
                //$data['file'][] = $file;
                $data['file'][] = array(
                	'file_name' => $file,
                	'percentage' => floor($p.
                    "%")
                );
                //$this->load->view('search',$data);

            }

        }
    }

    if ($p < 50) {
        $data['absmessage'] = "Not found";

    }
    $this->load->view('co_studentdetails', $data);
}



public function select_abs()
{
	/*$file=$this->uri->segment(3);*/
	$file=$_POST['file'];
echo "<h2>".$file."</h2>";
	$this->load->model('staff_model');


$fl='uploads/'.$file;
$content = $this->staff_model->read_file_docx($fl);
if($content !== false) {

   $sfile= nl2br($content);
   $data=$sfile;
   echo "<h3>".$data."</h3>";
   //$data1['selectfile2']='';
}
else {
    echo ' Please check that file.';
}
}


public function statusapproved()
{
	$filename = $this->uri->segment(3);
	$sid=$this->uri->segment(4);
	$this->load->model('staff_model');
	$this->staff_model->statusapproved($filename);
	$this->staff_model->sta_update($filename,$sid);
	$this->session->set_flashdata('error','Project Approved');
	redirect(base_url().'co_studentdetails');
}
public function statusrejected()
{
	$filename = $this->uri->segment(3);

	$this->load->model('staff_model');
	$this->staff_model->statusrejected($filename);
$this->session->set_flashdata('error','Project Rejected');
	redirect(base_url().'co_studentdetails');
}


public function co_show_noti()
{
	$this->load->model('staff_model');
	$data['notification']=$this->staff_model->show_not();
	$this->load->view('co_notification',$data);
}


public function updatenoti()
{
	$rid=$this->session->userdata('Rid');
	$this->load->model('my_model');
	$this->my_model->updatenoti($rid);
	redirect(base_url().'home');
}

public function set_date()
{
	$this->form_validation->set_rules('sdate','Sdate','required');
	$this->form_validation->set_rules('edate','Edate','required');
	$this->load->model('staff_model');
	$data=array('startdate'=>$this->input->post('sdate'),'enddate'=>$this->input->post('edate'));
		$this->staff_model->insertdate($data);
		$this->session->set_flashdata('error','Date Submitted');
		redirect(base_url().'co_notification');
					
}

	public function downloadabs()
	{
		$filename = $this->uri->segment(3);

		$this->load->helper('download');

		$data = file_get_contents("./uploads/".$filename); // Read the 
		force_download($file_name, $data);
		$this->load->view('my_documents');
	}

	public function abswithdrowal()
	{
		$filename = $this->uri->segment(3);
		$this->load->model('my_model');
		$this->my_model->abswithdrowal($filename);
		$this->session->set_flashdata('error','Abstract withdrawn');
		redirect(base_url().'my_documents');
	
	}

	public function insertcomments()
	{
		$this->form_validation->set_rules('comments','comments','required');
		$filename = $this->uri->segment(3);
		$this->load->model('staff_model');
		$comm=$this->input->post('comments');
		$this->staff_model->insertcomments($filename,$comm);
		$this->session->set_flashdata('error','Comments sent');
		redirect(base_url().'search');
	}
}
