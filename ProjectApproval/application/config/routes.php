<?php
defined('BASEPATH') OR exit('No direct script access allowed');




$route['index'] = 'Welcome';
$route['mainlogin'] = 'Welcome/mainlogin';
$route['home'] = 'Welcome/home';
$route['approval_status'] = 'Welcome/approval_status';
$route['review_status'] = 'Welcome/review_status';
$route['profile'] = 'Welcome/profile';
$route['my_documents'] = 'Welcome/my_documents';
$route['abstract_submission'] = 'Welcome/abstract_submission';
$route['notification'] = 'Welcome/notification';
$route['change_password'] = 'Welcome/change_password';
$route['guidehome'] = 'Welcome/guidehome';
$route['registrationform'] = 'Welcome/registrationform';
$route['studform'] = 'Welcome/studform';
$route['guideprofile'] = 'Welcome/guideprofile';
$route['staff_profile'] = 'Welcome/staff_profile';
$route['studentdetails'] = 'Welcome/studentdetails';
$route['guidenotification'] = 'Welcome/guidenotification';

$route['coordinatorhome'] = 'Welcome/coordinatorhome';
$route['coordinatorprofile'] = 'Welcome/coordinatorprofile';
$route['co_guideallocation'] = 'Welcome/co_guideallocation';
$route['co_studentdetails'] = 'Welcome/co_studentdetails';
$route['co_notification'] = 'Welcome/co_notification';
$route['staff_changepassword'] = 'Welcome/staff_changepassword';
$route['adminlogin'] = 'Welcome/adminlogin';
$route['adminhome'] = 'Welcome/adminhome';
$route['admin_pending_staff'] = 'Welcome/admin_pending_staff';
$route['admin_registered_staff'] = 'Welcome/admin_registered_staff';
$route['admin_set_coordinator'] = 'Welcome/admin_set_coordinator';
$route['staffhome'] = 'Welcome/staffhome';
$route['co_pending_stud'] = 'Welcome/co_pending_stud';
$route['co_registered_stud'] = 'Welcome/co_registered_stud';
$route['staff_stud_details'] = 'Welcome/staff_stud_details';
$route['co_staff_details'] = 'Welcome/co_staff_details';
$route['co_guide'] = 'Welcome/co_guide';
$route['staff_validation'] = 'Welcome/staff_validation';
$route['search'] = 'Welcome/search';
$route['printabs'] = 'Welcome/printabs';




/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
