<?php
class my_model extends CI_model
{
	function can_login($user,$pass)
	{
		$this->db->where('Username',$user);
		$this->db->where('Password',$pass);
		//$this->db->where('Approvedstatus','approved');

		$query=$this->db->get('Registration');

		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	function can_change($rid,$cupass,$data)
	{
		$this->db->where('Rid',$rid);
		$this->db->where('Password',$cupass);
		$this->db->update('Registration',array('Password' => $data));
		return true;
		
	}
	function staff_can_change($rid,$cupass,$data)
	{
		$this->db->where('Rid',$rid);
		$this->db->where('Password',$cupass);
		$this->db->update('Registration',array('Password' => $data));
		return true;
		
	}
	function insert($data)
	{
		$this->db->insert('Registration',$data);
	}
	function abstract_insert($data)
	{

		$this->db->insert('Abstract',$data);
	}

	function fetch_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype'=>'student','Approvedstatus'=>'pending'));
		return $query;

	}
	function fetch_registered_data()
	{
		$sql="select registration.*,abstract.AbstractFile from registration inner join abstract on registration.Rid in (abstract.RegNo) WHERE abstract.Status='approved'";
		$query=$this->db->query($sql);
		
		return $query;
	
	}
	function stud_delete($id)
	{
		$this->db->where('Rid',$id);
		$this->db->delete('Registration');
	}
	
	function getrid($user)
	{
		$this->db->where('Username',$user);


		
		$query=$this->db->get('Registration');

		if($query->num_rows() > 0)
		{
			return $query->row()->Rid;
		}
		else
		{
			return false;
		}
	}

	function insertNotice($arrayOfNoticeFiles)
	{
		$tableName  =  "upload";
		$inputArray =  $arrayOfNoticeFiles;

		$data = array(

                'upload'   =>   $inputArray["getfiledata_file_name"]
		);

		$this->db->insert($tableName,$data);
	}

	function pro_data($rid)
	{
		$sql="SELECT * FROM `registration` WHERE Rid='$rid'";
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}


		function notificationshow($rid)
	{
		$this->db->where('StudentId',$rid);
		$this->db->where('status','1');
		$query=$this->db->get('guidenotification');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function not_message($rid)
	{
		$this->db->where('StudentId',$rid);
		
		$query=$this->db->get('guidenotification');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function selectabstract($rid)
	{
		$sql="SELECT * FROM `abstract` WHERe STATUS in(SELECT Status from abstract WHERE RegNo='$rid')and RegNo='$rid' and Status not in ('rejected') and Status not in ('withdrawal')";

		$result=$this->db->query($sql);
				if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}



	function absresult($absfile,$rid)
	{
		$this->db->where('RegNo',$rid);
		$this->db->where('AbstractFile',$absfile);
		$result=$this->db->get('abstract');
		if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}


	function updatenoti($rid)
	{
		$this->db->where('StudentId',$rid);
		$result=$this->db->update('guidenotification',array('status' => '0'));
		return $result;
	}

	function Approvedstatus($rid)
	{
		$sql="SELECT * FROM `abstract` WHERE RegNo='$rid'";
		$result=$this->db->query($sql);
		if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}

		function std_notification()
	{
		$this->db->where('Student','1');
		$result=$this->db->get('notification');
		if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}

	function abswithdrowal($filename)
	{
		$this->db->where('AbstractFile',$filename);
		$result=$this->db->update('abstract',array('status' => 'withdrawal'));
		return $result;
	}

}
?>