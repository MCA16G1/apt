<?php
class staff_model extends CI_model
{
	function can_login($rid)
	{

		$this->db->where('Rid',$rid);
		$query=$this->db->get('Staff');

		if($query->num_rows()>0)
		{
			return $query->row()->Usertype;
		}
		else
		{
			return false;
		}
	}
	function guide_insert($data)
	{
		$this->db->insert('staff',$data);
	}

	function fetch_guide_data()
	{
		$this->db->select('Registration.Fname,Registration.Year,Registration.Username,Staff.Rid');;
		$this->db->from('Registration');
		$this->db->join('Staff','Staff.Rid = Registration.Rid');
		$this->db->where('Staff.Usertype','guide');

		$query=$this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

		function fetch_registered_data()
	{
		$sql="SELECT * FROM `registration` WHERE Usertype='staff' AND Approvedstatus ='approved' AND Rid NOT in(select Rid from staff)";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function notification($rid)
	{
		$this->db->where('Rid',$rid);
		$this->db->where('Usertype','coordinator');
		$query=$this->db->get('Staff');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	function guidenotification($rid)
	{
		$this->db->where('Rid',$rid);
		$this->db->where('Usertype','guide');
		$query=$this->db->get('Staff');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	function guide_delete($id)
	{
		$this->db->where('Rid',$id);
		$this->db->delete('Staff');
	}
	function notify_insert($data)
	{
		$this->db->insert('notification',$data);
	}

	function guidedata()
	{
		$this->db->select('Registration.Fname,Staff.Rid');;
		$this->db->from('Registration');
		$this->db->join('Staff','Staff.Rid = Registration.Rid');
		$this->db->where('Staff.Usertype','guide');

		$query=$this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	function std_guide_data()
	{
	$sql="SELECT * from registration WHERE Rid not in(select StudentID from guideallocation) and Usertype='student'";
	$query=$this->db->query($sql);

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function selectstddata($id)
	{
		$this->db->select('*');
		$this->db->where_in('Rid',$id);
		$query=$this->db->get('Registration');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	function selectguidedata($gid)
	{
		$this->db->select('*');
		$this->db->where_in('Rid',$gid);
		$query=$this->db->get('Registration');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function insertallocation($data,$gdata)
	{
		foreach ($data as $row) 
		{
			$sid=$row->Rid;
			$sname=$row->Fname;
			foreach($gdata as $grow)
			{
				$gid=$grow->Rid;
				$gname=$grow->Fname;
				$sql="insert into guideallocation (StudentId, StudentName, GuideId, GuideName) values('$row->Rid','$row->Fname','$grow->Rid', '$grow->Fname')";
				$this->db->query($sql);
			}
			
		}
	}
	function guide_data($rid)
	{
		
		$this->db->where('Rid',$rid);
		$query=$this->db->get('Registration');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function std_data($rid)
	{
		$this->db->where('GuideID',$rid);
		$query=$this->db->get('guideallocation');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}

	function isert_not($data,$id,$title,$gid)
	{
		foreach ($id as $row) {
			$id=$row;
			$sql="insert into guidenotification (GuideId,title,Message,StudentId,status)values('$gid','$title','$data','$id',1)";

			$this->db->query($sql);
		}
	}

	function searchfile($rid)
{
	$query="SELECT AbstractFile,RegNo from abstract where RegNo in(select StudentID from guideallocation where GuideID='$rid' and status='pending')";
	$result=$this->db->query($query);
			if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}

}
function searchstddata($rid)
{
	$this->db->where('GuideID',$rid);

		$query=$this->db->get('guideallocation');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
}



function read_file_docx($filename){
	

    $striped_content = '';
    $content = '';

    if(!$filename || !file_exists($filename)) return false;

    $zip = zip_open($filename);


   // if (!$zip || is_numeric($zip)) return false;

    while ($zip_entry = zip_read($zip)) {

        if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

        if (zip_entry_name($zip_entry) != "word/document.xml") continue;

        $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

        zip_entry_close($zip_entry);
    }// end while

    zip_close($zip);

    //echo $content;
    //echo "<hr>";
    //file_put_contents('1.xml', $content);

    $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
    $content = str_replace('</w:r></w:p>', "\r\n", $content);
    $striped_content = strip_tags($content);

    return $striped_content;
}


	function gutabs($filename)
	{
		$sql="SELECT AbstractFile from abstract where AbstractFile NOT in('$filename')";
		$result=$this->db->query($sql);
			if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}

	}

	function statusapproved($filename)
	{
		$this->db->where('AbstractFile',$filename);
	
		$this->db->update('abstract',array('Status'=>"Approved"));

	}

	function sta_update($filename,$sid)
	{
		$sql="UPDATE abstract set Status='Rejected' WHERE AbstractFile NOT IN('$filename') AND RegNo='$sid'";
		$result=$this->db->query($sql);
		return $result;
	}


	function statusrejected($filename)
	
	{
		$this->db->where('AbstractFile',$filename);
		
		$this->db->update('abstract',array('Status'=>"Rejected"));

	}

	function show_not()
	{
		
	}

	function search_abstract()
	{
		$sql="select * from registration WHERE rid in(SELECT RegNo from abstract) and Approvedstatus ='approved'";
		$result=$this->db->query($sql);
		if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
		
	}

	function abstract__std()
	{
		$query="SELECT AbstractFile,RegNo,guidecomments from abstract where status='pending'";
	$result=$this->db->query($query);
			if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}

	}

	function gui_notification()
	{
		$this->db->where('guide','1');
		$result=$this->db->get('notification');
		if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}




	function insertdate($data)
	{
		$this->db->update('setdate',$data);
	}

	function selectdate()
	{
		$result=$this->db->get('setdate');
				if($result->num_rows() > 0)
		{
			return $result->result();
		}
		else
		{
			return false;
		}
	}

	function registered_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype'=>'student','Approvedstatus'=>'approved'));
		return $query;
	}


	function insertcomments($filename,$comm)
	{
		$this->db->where('AbstractFile',$filename);
		$this->db->update('abstract',array('guidecomments'=>$comm));
		return true;
	}


}
?>