<?php
class admin_model extends CI_model
{
	function can_login($aduser,$adpass)
	{
		$this->db->where('Username',$aduser);
		$this->db->where('Password',$adpass);

		$query=$this->db->get('admin');

		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function fetch_data()
	{
		$query=$this->db->get_where('Registration',array('Usertype' => 'staff','Approvedstatus'=>'pending'));
		return $query;
	}

	function fetch_registered_data()
	{
		$sql="SELECT * FROM `registration` WHERE Usertype='staff' AND Approvedstatus ='approved'";
		$query=$this->db->query($sql);
		return $query;
	}
	function update($id)
	{
		$this->db->where('Rid',$id);
		$this->db->update('Registration',array('Approvedstatus'=>"approved"));
		return true;

	}
	function reject($id)
	{
		$this->db->where('Rid',$id);
		$this->db->update('Registration',array('Approvedstatus'=>"rejected"));
		return true;

	}
	function testing_insert($data)
	{
		$this->db->insert('Staff',$data);
	}
	
	function delete($id)
	{
		$this->db->where('Rid',$id);
		$this->db->delete('Registration');
	}

	function testingcood()
	{
		$this->db->where('Usertype','coordinator');
		$query=$this->db->get('staff');
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

		function deletecood()
	{
		$this->db->where('Usertype','coordinator');
		$this->db->delete('staff');
	}
}
?>